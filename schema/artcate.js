// 1. 导入定义验证规则的模块
const joi = require("joi");

// 2/ 定义 name 和 alias 的校验规则
const name = joi.string().required();
const alias = joi.string().alphanum().required();

// 向外共享验证规则对象
exports.addCateSchema = {
  body: {
    name,
    alias,
  },
};
const id = joi.number().integer().min(1).required()
exports.deleteCateSchema = {
  params: {
    id
  }
}

exports.getCateSchema = {
  params: {
    id
  }
}

// 向外共享更新分类的规则对象
exports.updateCateSchema = {
  body: {
    id,
    name,
    alias
  }
}