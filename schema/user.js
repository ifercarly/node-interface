const joi = require("joi");

/**
 * string() 值必须是字符串
 * alphanum() 值只能是包含 a-zA-Z0-9 的字符串
 * min(length) 最小长度
 * max(length) 最大长度
 * required() 值是必填项，不能为 undefined
 * pattern(正则表达式) 值必须符合正则表达式的规则
 */

// 验证用户名、密码的规则
const username = joi.string().alphanum().min(3).max(10).required();
const password = joi
  .string()
  .pattern(/^[\S]{6,12}$/)
  .required();

// 登录和注册表单的验证规则对象
exports.regLoginSchema = {
  // 表示需要对 req.body 中的数据进行验证
  body: {
    username,
    password,
  },
};

// 定义 id, nickname, emial 的验证规则
const id = joi.number().integer().min(1).required();
const nickname = joi.string().required();
const email = joi.string().email().required();
exports.updateUserinfoSchema = {
  body: {
    id,
    nickname,
    email,
  },
};
exports.updatePasswordSchema = {
  body: {
    oldPwd: password,
    newPwd: joi.not(joi.ref("oldPwd")).concat(password),
  },
};
// 验证头像数据
const avatar = joi.string().dataUri().required();

// 验证规则对象 - 更新头像
exports.updateAvatarSchema = {
  body: {
    avatar,
  },
};
