const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const db = require("../db/index.js");
const config = require("../config.js");

exports.regUser = (req, res) => {
  const userInfo = req.body;
  /* if (!userInfo.username || !userInfo.password) {
    return res.sd("用户名或密码不能为空！");
  } */
  const sql = `select * from users where username=?`;
  db.query(sql, userInfo.username, (err, results) => {
    if (err) {
      return res.sd(err);
    }
    if (results.length > 0) {
      return res.sd("用户名被占用，请更换其他用户名");
    }
    userInfo.password = bcrypt.hashSync(userInfo.password, 10);
    const sql = "insert into users set ?";
    // 调用 db.query() 执行 SQL 语句
    db.query(sql, { username: userInfo.username, password: userInfo.password }, (err, results) => {
      if (err) return res.sd(err);
      if (results.affectedRows !== 1) return res.sd("注册用户失败，请稍后再试！");
      // 注册成功
      res.send({ status: 0, message: "注册成功" });
    });
  });
};

// 登录的处理函数
exports.login = (req, res) => {
  const userInfo = req.body;
  const sql = `select * from users where username=?`;
  // 执行 sql 语句，根据用户名查询用户的信息
  db.query(sql, userInfo.username, (err, results) => {
    // 指定 sql 失败
    if (err) return res.sd(err);
    // 执行 sql 语句成功，但是获取到的数据条数不等于 1
    if (results.length !== 1) return res.sd("登录失败");
    // 判断用户名和密码是否正确
    // 将用户输入的密码和数据库中存储的密码进行比较
    const compareResult = bcrypt.compareSync(userInfo.password, results[0].password);
    // 根据对比后的结果进行判断
    if (!compareResult) return res.sd("登录失败！");
    // #1
    const user = { ...results[0], password: "", user_pic: "" };
    // #2
    const tokenStr = jwt.sign(user, config.jwtSecretKey, { expiresIn: config.expiresIn });
    // #3
    res.send({
      status: 0,
      message: "登录成功！",
      token: "Bearer " + tokenStr,
    });
  });
};
