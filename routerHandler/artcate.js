const db = require("../db/index");

// 获取文章分类列表数据的处理函数
exports.getArticleCates = (req, res) => {
  // 定义查询分类列表数据的 SQL 语句
  // is_delete 为 0 表示没有被 标记为删除 的数据
  const sql = "select * from article_cate where is_delete=0 order by id asc";

  // 调用 db.query() 执行 SQL 语句
  db.query(sql, (err, results) => {
    // SQL 语句执行失败
    if (err) return res.sd(err);

    // SQL 语句执行成功
    res.send({
      status: 0,
      message: "获取文章分类列表成功！",
      data: results,
    });
  });
};
exports.addArticleCates = (req, res) => {
  const sql = `select * from article_cate where name=? or alias=?`;
  db.query(sql, [req.body.name, req.body.alias], (err, results) => {
    // 执行 SQL 语句失败
    if (err) return res.sd(err);

    // 分类名称 和 分类别名 都被占用
    if (results.length === 2) return res.sd("分类名称与别名分别被占用，请更换后重试！");
    if (results.length === 1 && results[0].name === req.body.name && results[0].alias === req.body.alias) return res.sd("分类名称与别名同时被占用，请更换后重试！");
    // 分类名称 或 分类别名 被占用
    if (results.length === 1 && results[0].name === req.body.name) return res.sd("分类名称被占用，请更换后重试！");
    if (results.length === 1 && results[0].alias === req.body.alias) return res.sd("分类别名被占用，请更换后重试！");

    // #1 定义插入文章分类的 sql 语句
    const sql = `insert into article_cate set ?`;

    // #2 执行插入文章分类的 sql 语句
    db.query(sql, req.body, (err, results) => {
      // SQL 语句执行失败
      if (err) return res.sd(err);

      // SQL 语句执行成功，但是影响行数不等于 1
      if (results.affectedRows !== 1) return res.sd("新增文章分类失败！");

      // 新增文章分类成功
      res.sd("新增文章分类成功！", 0);
    });
  });
};

// 删除文章分类的处理函数
exports.deleteCateById = (req, res) => {
  // 1. 定义删除的 sql 语句
  const sql = `update article_cate set is_delete=1 where id=?`;

  // 2. 执行删除的 sql 语句
  db.query(sql, req.params.id, (err, results) => {
    // SQL 语句执行失败
    if (err) return res.sd(err);

    // 语句执行 SQL 成功，但是影响行数不等于 1
    if (results.affectedRows !== 1) return res.sd("删除文章分类失败！");

    // 删除文章分类成功
    res.sd("删除文章分类成功！", 0);
  });
};

// 根据 Id 获取文章分类的处理函数
exports.getArticleCateById = (req, res) => {
  // 1. 定义根据 id 获取文章分类的 sql 语句
  const sql = `select * from article_cate where id=?`;

  // 2. 执行查询的 sql 语句
  db.query(sql, req.params.id, (err, results) => {
    // 执行 SQL 语句失败
    if (err) return res.sd(err);

    // SQL 语句执行成功，但是没有查询到任何数据
    if (results.length !== 1) return res.sd("获取文章分类数据失败！");

    // 把数据响应给客户端
    res.send({
      status: 0,
      message: "获取文章分类数据成功！",
      data: results[0],
    });
  });
};

exports.updateCateById = (req, res) => {
  const sql = `select * from article_cate where Id<>? and (name=? or alias=?)`;
  db.query(sql, [req.body.id, req.body.name, req.body.alias], (err, results) => {
    // 执行 SQL 语句失败
    if (err) return res.sd(err);

    // 分类名称 和 分类别名 都被占用
    if (results.length === 2) return res.sd("分类名称与别名分别被占用，请更换后重试！");
    if (results.length === 1 && results[0].name === req.body.name && results[0].alias === req.body.alias) return res.sd("分类名称与别名同时被占用，请更换后重试！");
    // 分类名称 或 分类别名 被占用
    if (results.length === 1 && results[0].name === req.body.name) return res.sd("分类名称被占用，请更换后重试！");
    if (results.length === 1 && results[0].alias === req.body.alias) return res.sd("分类别名被占用，请更换后重试！");

    // #1 更新分类的 sql 语句
    const sql = `update article_cate set ? where id=?`;

    // #2 执行 sql 语句
    db.query(sql, [req.body, req.body.id], (err, results) => {
      // SQL 语句执行失败
      if (err) return res.sd(err);

      // SQL 语句执行成功，但是影响行数不等于 1
      if (results.affectedRows !== 1) return res.sd("更新文章分类失败！");

      // 更新文章分类成功
      res.sd("更新文章分类成功！", 0);
    });
  });
};

