const bcrypt = require("bcryptjs");
// 导入数据库操作模块
const db = require("../db/index");

// 创建用户基本信息的处理函数
exports.getUserInfo = (req, res) => {
  // 定义查询用户信息的 sql 语句
  const sql = `select id, username, nickname, email, user_pic from users where id=?`;

  // 调用 db.query() 执行 sql 语句
  db.query(sql, req.auth.id, (err, results) => {
    // 执行 sql 语句失败
    if (err) return res.sd(err);

    // 执行的 sql 语句成功，但是查询的结果可能为空
    if (results.length !== 1) return res.sd("获取用户信息失败！");

    // 用户信息获取成功
    res.send({
      status: 0,
      message: "获取用户基本信息成功！",
      data: results[0],
    });
  });
};
// 更新用户基本信息的处理函数
exports.updateUserInfo = (req, res) => {
  // 定义更新用户信息的 sql 语句
  const sql = `update users set ? where id=?`;

  // 调用 db.query() 执行 sql 语句
  db.query(sql, [req.body, req.body.id], (err, results) => {
    // 执行 sql 语句失败
    if (err) return res.sd(err);

    // 执行 sql 语句成功，但影响函数不为 1、
    if (results.affectedRows !== 1) return res.sd("修改用户基本信息失败！");

    // 修改用户信息成功
    return res.sd("修改用户基本信息成功！", 0);
  });
};
exports.updatePassword = (req, res) => {
  // 执行根据 id 查询用户数据的 SQL 语句
  const sql = `select * from users where id=?`;

  // 执行 SQL 语句查询用户是否存在
  db.query(sql, req.auth.id, (err, results) => {
    // 执行 SQL 语句失败
    if (err) return res.sd(err);

    // 判断结果是否存在
    if (results.length !== 1) return res.sd("用户不存在！");

    // 判断用户输入的旧密码是否正确

    const compareResult = bcrypt.compareSync(req.body.oldPwd, results[0].password);
    if (!compareResult) return res.sd("旧密码错误！");

    // #1 定义更新密码的 SQL 语句
    const sql = `update users set password=? where id=?`;

    // #2 对新密码进行加密处理
    const newPwd = bcrypt.hashSync(req.body.newPwd, 10);

    // #3 执行 SQL 语句，根据 id 更新用户的密码
    db.query(sql, [newPwd, req.auth.id], (err, results) => {
      //语句执行失败
      if (err) return res.sd(err);

      // 语句执行成功，但是影响行数不等于 1
      if (results.affectedRows !== 1) return res.sd("更新密码失败！");

      // 更新密码成功
      res.sd("更新密码成功！", 0);
    });
  });
};

exports.updateAvatar = (req, res) => {
  // 更新用户头像的 sql 字段
  const sql = "update users set user_pic=? where id=?";

  db.query(sql, [req.body.avatar, req.auth.id], (err, results) => {
    // SQL 语句失败
    if (err) return res.sd(err);

    // SQL 语句成功，但是影响行数不等于 1
    if (results.affectedRows !== 1) return res.sd("更新头像失败！");

    // 更新用户头像成功
    return res.sd("更新头像成功！", 0);
  });
};
