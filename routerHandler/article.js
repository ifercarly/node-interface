const path = require("path");

exports.addArticle = (req, res) => {
  // 判断客户端是否提交了 封面图片
  if (!req.file || req.file.fieldname !== "cover_img") return res.sd("文章封面是必选参数！");
  const articleInfo = {
    // 标题、内容、状态、所属的分类 Id
    ...req.body,
    // 文章封面在服务器端的存放路径
    cover_img: path.join(__dirname, "../uploads", req.file.filename),
    // 文章发布时间
    pub_date: new Date(),
    // 文章作者的Id
    author_id: req.auth.id,
  };
  // #1 定义发布文章的 sql 语句
  const sql = `insert into articles set ?`;

  // #2 导入数据库操作模块
  const db = require("../db/index");

  // #3 执行 SQL 语句
  db.query(sql, articleInfo, (err, results) => {
    if (err) return res.sd(err);
    // 执行 SQL 语句成功，但是影响行数不等于 1
    if (results.affectedRows !== 1) return res.sd("发布文章失败！");

    // 发布文章成功
    res.sd("发布文章成功", 0);
  });
};
