const express = require("express");
const router = express.Router();
const userHandler = require("../routerHandler/user");
// 导入验证表单数据的中间件
const expressJoi = require('@escook/express-joi')
// 导入需要的验证规则对象
const { regLoginSchema } = require('../schema/user')
router.post("/register", expressJoi(regLoginSchema), userHandler.regUser);

router.post("/login", expressJoi(regLoginSchema), userHandler.login);

module.exports = router;
