const express = require("express");
const expressJoi = require("@escook/express-joi");
const router = express.Router();

const userInfoHandler = require("../routerHandler/userinfo.js");
const { updateUserinfoSchema, updatePasswordSchema, updateAvatarSchema } = require("../schema/user");

router.get("/userinfo", userInfoHandler.getUserInfo);
router.post("/userinfo", expressJoi(updateUserinfoSchema), userInfoHandler.updateUserInfo);
router.post("/updatepwd", expressJoi(updatePasswordSchema), userInfoHandler.updatePassword);
router.post("/update/avatar", expressJoi(updateAvatarSchema), userInfoHandler.updateAvatar);

module.exports = router;
