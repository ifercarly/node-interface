// #1
const path = require("path");
// #2
const multer = require("multer");
const express = require("express");
// #1
const expressJoi = require('@escook/express-joi')
const router = express.Router();
const articleHandler = require("../routerHandler/article");
// #2
const { addArticleSchema } = require('../schema/article')

const upload = multer({ dest: path.join(__dirname, "../uploads") });

// #3
// 先使用 multer 解析表单数据
// 再使用 expressJoi 对解析的表单数据进行验证
router.post("/add", upload.single('cover_img'), expressJoi(addArticleSchema), articleHandler.addArticle);

module.exports = router;
