const express = require("express");
const expressJoi = require("@escook/express-joi");
const artcateHandler = require("../routerHandler/artcate");
const { addCateSchema, deleteCateSchema, getCateSchema, updateCateSchema } = require("../schema/artcate");

// 创建路由对象
const router = express.Router();

// 获取文章分类的列表数据
router.get("/cates", artcateHandler.getArticleCates);
router.post("/addcates", expressJoi(addCateSchema), artcateHandler.addArticleCates);
router.get('/deletecate/:id', expressJoi(deleteCateSchema), artcateHandler.deleteCateById)
router.get('/cates/:id', expressJoi(getCateSchema), artcateHandler.getArticleCateById)
router.post('/updatecate', expressJoi(updateCateSchema), artcateHandler.updateCateById)

// 向外共享路由对象
module.exports = router;
